var express = require('express');
var proxy = require('http-proxy-middleware');
var path = require('path');

var app = express();

app.use(function (req, res, next) {

  req.headers = {};

  req.headers['Accept-Encoding'] = 'gzip, deflate, br';
  req.headers['Accept-Language'] = 'en-GB,en-US;q=0.9,en;q=0.8';
  req.headers['Upgrade-Insecure-Requests'] = '1';
  req.headers['Host'] = 'www.fractal.live';
  req.headers['User-Agent'] = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36';
  req.headers['Accept'] = 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8';
  req.headers['Cache-Control'] = 'max-age=0';
  req.headers['Cookie'] = '__unam=31edb77-1629f494b08-60d5275c-6; _ga=GA1.2.17108314.1523088514; _gid=GA1.2.1126346023.1523088514; connect.sid=s:JCGSRa36dzqaeR0c5HQThyQa6B8nJ6XM.xy+LJClRrDBPSkPTJLhP8s9628wRnw/YWnEu9XT8vwQ';
  req.headers['Connection'] = 'keep-alive';

  req.headers['If-None-Match'] = 'W/"672f3-5X75ZN7givUZ/31QS+xATfDufKU"';

  next();
})

app.use('/api', proxy({
  target: 'https://www.fractal.live',
  changeOrigin: true,
  pathRewrite: {
    '^/api' : '/designs',     // rewrite path
  },
}));

app.use(express.static('public'));
app.get('*', function(req, res) {
  res.sendFile(path.join(__dirname, 'public/index.html'));
});


app.listen(process.env.PORT || 3000, function () {
  console.log('Ready', process.env.PORT || 3000);
});
